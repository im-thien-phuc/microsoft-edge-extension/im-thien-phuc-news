var request = new XMLHttpRequest();
request.open('GET', 'https://imthienphuc.com/wp-json/wp/v2/posts', true);
request.onload = function () {

    // Begin accessing JSON data here
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
        var postsContainer = document.getElementById('posts');
        for (let i = 1; i <= 10; i++) {
            var article = document.createElement('article');
            article.setAttribute('class', 'post');
            article.innerHTML = `<h2 class="post-title">${i}. ${data[i].title.rendered}</h2>`;
            postsContainer.appendChild(article);
        }
    } else {
        const errorMessage = 'Not found posts';
        var postsContainer = document.getElementById('posts');
        postsContainer.innerHTML = errorMessage;
    }
}
request.send();